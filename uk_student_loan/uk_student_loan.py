"""Calculate statistics about UK Plan 2 (2012 and later) student loan."""

import collections
import datetime
import math


MIN_SALARY = 25000
MAX_INTEREST_SALARY = 45000
MAX_EXTRA_INTEREST = 0.03
REPAY_PERCENTAGE = 0.09
# RPIs from www.studentloanrepayment.co.uk/portal/page?_pageid=93,6678755&_dad=portal
RPIS = {2012: 0.036, 2013: 0.033, 2014: 0.025, 2015: 0.009, 2016: 0.016,
        2017: 0.031}
PAYMENT_MONTHS = {1: [10], 2: [10, 2], 3: [10, 2, 5]}
REPAYMENT_MAX_LENGTH_YEARS = 30

RepaymentStats = collections.namedtuple(
    "RepaymentStats", ["days_to_repay", "loan_yearly_values", "paid_total"])


class Loan(object):
  """A class for storing a loan and performing typical operations with it."""
  def __init__(self, initial_value=0.0):
    self._loan = initial_value

  def __str__(self):
    return str(self._loan)

  def accrue_monthly_interest(self, annual_interest_rate):
    """Accrues the monthly interest."""
    self._loan *= (1.0 + annual_interest_rate)**(1.0 / 12.0)

  def accrue_daily_interest(self, annual_interest_rate):
    """Accrues the daily interest."""
    self._loan *= (1.0 + annual_interest_rate)**(1.0 / 365.0)

  def accrue_interest(self, interest_rate):
    """Accrues the given interest."""
    self._loan *= 1.0 + interest_rate

  def increase(self, amount):
    """Increase the loan by the given positive amount."""
    assert amount >= 0
    self._loan += amount

  def repay(self, amount):
    """Reduces the loan value by the given positive amount."""
    assert amount >= 0
    self._loan -= amount

  def is_repaid(self):
    """Returns True is the value of the loan is smaller or equal to 0."""
    return self._loan <= 0

  @property
  def value(self):
    """Returns the value of the loan."""
    return self._loan


def monthly_repayment(salary):
  """Calculates monthly repayment for the given salary."""
  if salary <= MIN_SALARY:
    return 0.0
  return math.floor((salary - MIN_SALARY) * REPAY_PERCENTAGE) / 12.0


def interest_rate_for_salary(salary):
  """Calculates interest rate for the given salary."""
  if salary <= MIN_SALARY:
    return 0.0
  # Linearly increases from 0.0 to MAX_EXTRA_INTEREST.
  elif salary <= MAX_INTEREST_SALARY:
    linear_factor = (salary - MIN_SALARY) / (MAX_INTEREST_SALARY - MIN_SALARY)
    return linear_factor * MAX_EXTRA_INTEREST
  return MAX_EXTRA_INTEREST


def get_interest_rate(salary, year, is_student, rpi_fn=lambda year: RPIS[year]):
  """Calculates the interest rate for the given status and salary."""
  if is_student:
    return rpi_fn(year) + MAX_EXTRA_INTEREST
  return rpi_fn(year) + interest_rate_for_salary(salary)


def calculate_loan(yearly_fee, course_years, start_year, terms_per_year):
  """Calculates the approximate loan (due to unknown exact dates when money was
  sent to the university)."""
  assert terms_per_year <= 3
  per_term_fee = yearly_fee / terms_per_year
  study_end_date = datetime.date(year=start_year + course_years, month=7, day=1)
  # The repayment doesn't start until March 1 -- the start of the tax year.
  repayment_start_date = datetime.date(
      year=start_year + course_years + 1, month=4, day=1)

  loan = Loan(0.0)
  loan_day = datetime.date(year=start_year, month=10, day=1)
  while loan_day <= repayment_start_date:
    interest_rate = get_interest_rate(
        salary=None, year=loan_day.year, is_student=True)
    loan.accrue_daily_interest(interest_rate)
    if (loan_day <= study_end_date
        and loan_day.month in PAYMENT_MONTHS[terms_per_year]
        and loan_day.day == 1):
      loan.increase(per_term_fee)  # Pay the fee for this term.
    loan_day += datetime.timedelta(days=1)
  return loan


def simulate_repayment(loan, salary_fn, start_year, rpi_fn):
  """Calculates how long the repayment takes, what will be the loan each
  year and how much will be SLC paid in total."""
  loan = Loan(loan.value)  # Copy the loan to keep the argument intact.
  loan_values = collections.OrderedDict()
  paid_total = 0
  for year in range(start_year, start_year + REPAYMENT_MAX_LENGTH_YEARS):
    loan_values[year] = loan.value
    years_since_began_working = year - start_year
    current_salary = salary_fn(years_since_began_working)
    interest_rate = get_interest_rate(
        salary=current_salary, year=year, is_student=False, rpi_fn=rpi_fn)
    for month in range(12):
      loan.repay(monthly_repayment(current_salary))
      paid_total += monthly_repayment(current_salary)

      if loan.is_repaid():  # Repaid at last.
        paid_total += loan.value  # Return overpaid money (value will be <= 0).
        return RepaymentStats(
            years_since_began_working * 12 + month, loan_values, paid_total)

      loan.accrue_monthly_interest(interest_rate)
  # The default sad case, loan "repaid" in 30 years.
  return RepaymentStats(
      REPAYMENT_MAX_LENGTH_YEARS * 12, loan_values, paid_total)

def simulate_repayment2(loan, salary_fn, start_year, rpi_fn):
  """Calculates how long the repayment takes, what will be the loan each
  year and how much will be SLC paid in total."""
  loan = Loan(loan.value)  # Copy the loan to keep the argument intact.
  loan_values = collections.OrderedDict()
  paid_total = 0

  # The repayment starts in the closest tax year after the studies.
  repay_day = datetime.date(year=start_year, month=4, day=1)
  max_repay_day = repay_day + datetime.timedelta(
      days=REPAYMENT_MAX_LENGTH_YEARS * 365)
  while repay_day < max_repay_day:
    if repay_day.month == 1 and repay_day.day == 1:
      loan_values[repay_day.year] = loan.value
    if repay_day.day == 1:
      years_in_employment = repay_day.year - start_year
      current_salary = salary_fn(years_in_employment)
      interest_rate = get_interest_rate(
          salary=current_salary, year=repay_day.year, is_student=False,
          rpi_fn=rpi_fn)
      loan.repay(monthly_repayment(current_salary))
      paid_total += monthly_repayment(current_salary)

      if loan.is_repaid():  # Repaid at last.
        paid_total += loan.value  # Return overpaid money (value will be <= 0).
        return RepaymentStats(
            (repay_day - datetime.date(year=start_year, month=4, day=1)).days,
            loan_values, paid_total)
    repay_day += datetime.timedelta(days=1)

    loan.accrue_daily_interest(interest_rate)

  # The default sad case: the rest of the loan forgiven in 30 years.
  return RepaymentStats(
      REPAYMENT_MAX_LENGTH_YEARS * 365, loan_values, paid_total)
