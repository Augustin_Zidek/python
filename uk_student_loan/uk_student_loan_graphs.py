"""Plot graphs about UK Plan 2 (2012 and later) student loan."""

import sys

import numpy as np
from matplotlib import pyplot as plt

import uk_student_loan


def postprocess_axes(axes, y_ticks, y_tick_labels, max_x_tick, y_label, title):
  """Performs visual and formatting tweaks on the axes."""
  axes.set_title(title)
  axes.set_yticks(y_ticks)
  for ytick in y_ticks:
    axes.axhline(y=ytick, ls="--", lw=0.5, color="black", alpha=0.3, zorder=-2)
  axes.set_yticklabels(y_tick_labels)

  x_ticks = range(0, max_x_tick, 10000)
  axes.set_xlim((0, max_x_tick))
  axes.set_xticks(x_ticks)
  axes.set_xticklabels(
      ["\u00a3{:,d}".format(xtick) for xtick in x_ticks],
      rotation=90, fontsize=8)
  for xtick in x_ticks:
    axes.axvline(x=xtick, ls="--", lw=0.5, color="black", alpha=0.3, zorder=-2)
  axes.axvline(x=uk_student_loan.MIN_SALARY, lw=1, color="black", alpha=0.5)

  for spine in axes.spines:
    axes.spines[spine].set_color((0, 0, 0, 0.3))
  axes.tick_params(axis='x', color=(0, 0, 0, 0.3))
  axes.tick_params(axis='y', color=(0, 0, 0, 0.3))

  axes.set_xlabel("Starting salary")
  axes.set_ylabel(y_label)
  axes.legend(title="Salary growth", loc="upper right")


def plot_repayments(loan, repayment_start_year, approx_future_rpi_fn,
                    salary_range, salary_growth_range):
  """Plots the salary-time and salary-total paid repayment graphs."""
  fig_time, time_fig_ax = plt.subplots()
  fig_money, money_fig_ax = plt.subplots()
  for salary_growth in salary_growth_range:
    repayment_times = []
    paid_totals = []
    for salary in salary_range:
      salary_fn = lambda year: salary * (1.0 + salary_growth)**year
      repayment_stats = uk_student_loan.simulate_repayment2(
          loan=loan,
          salary_fn=salary_fn,
          start_year=repayment_start_year,
          rpi_fn=approx_future_rpi_fn)
      repayment_times.append(repayment_stats.days_to_repay / 365.0)
      paid_totals.append(repayment_stats.paid_total)

    salary_growth_percent = salary_growth * 100
    time_fig_ax.plot(
        salary_range, repayment_times, label="%.0f%%" % salary_growth_percent)
    money_fig_ax.plot(
        salary_range, paid_totals, label="%.0f%%" % salary_growth_percent)

  postprocess_axes(
      time_fig_ax, y_ticks=range(0, 31, 5), y_tick_labels=range(0, 31, 5),
      max_x_tick=salary_range[-1] + 1, y_label="Repayment time (years)",
      title="Time to repay the loan of \u00a3{:,.0f}".format(loan.value))
  money_y_ticks = range(0, int(money_fig_ax.get_ylim()[1]) + 1, 10000)
  postprocess_axes(
      money_fig_ax, y_ticks=money_y_ticks,
      y_tick_labels=["\u00a3{:,d}".format(ytick) for ytick in money_y_ticks],
      max_x_tick=salary_range[-1] + 1, y_label="Total money repaid",
      title=("Total money paid to repay the loan of \u00a3{:,.0f}"
             .format(loan.value)))
  fig_time.tight_layout()
  fig_money.tight_layout()
  fig_time.savefig("repayment_times.png", dpi=600)
  fig_money.savefig("repayment_money.png", dpi=600)


def main(unused_args):
  """Plots the repayment times and total paid for different salaries."""
  course_years = 3
  course_start_year = 2012
  repayment_start_year = course_start_year + course_years
  terms = 3
  yearly_fee = 9000.0

  approx_future_rpi_fn = lambda _: 0.02

  loan = uk_student_loan.calculate_loan(
      yearly_fee=yearly_fee,
      course_years=course_years,
      start_year=course_start_year,
      terms_per_year=terms)

  plot_repayments(
      loan=loan,
      repayment_start_year=repayment_start_year,
      approx_future_rpi_fn=approx_future_rpi_fn,
      salary_range=np.arange(0, 150001, 1000),
      salary_growth_range=np.arange(0.0, 0.051, 0.01))


if __name__ == "__main__":
  main(sys.argv)
