"""Calculate personal UK Plan 2 (2012 and later) student loan."""
import sys

import uk_student_loan

def main(unused_args):
  """Calculates the loan repayment profile for the given inputs."""
  course_years = 4
  course_start_year = 2012
  repayment_start_year = course_start_year + course_years
  terms = 3
  yearly_fee = 9000.0

  salary = 31000
  approx_salary_growth = 0.02
  # Assume salary growth with a constant rate.
  salary_fn = lambda year: salary * (1 + approx_salary_growth)**year
  # Assume future RPI to be 2%.
  approx_future_rpi_fn = lambda _: 0.02

  loan = uk_student_loan.calculate_loan(
      yearly_fee=yearly_fee,
      course_years=course_years,
      start_year=course_start_year,
      terms_per_year=terms)
  repayment_stats = uk_student_loan.simulate_repayment(
      loan=loan,
      salary_fn=salary_fn,
      start_year=repayment_start_year,
      rpi_fn=approx_future_rpi_fn)
  repayment_months, loan_values, paid_total = repayment_stats
  paid_extra = 0.0 if paid_total <= loan.value else (paid_total - loan.value)
  if paid_total <= loan.value:
    paid_extra_percent = 0.0
  else:
    paid_extra_percent = paid_extra / paid_total * 100

  print("Your loan will be about    : %.0f GBP\n" % loan.value)
  print("Assuming %.0f%% annual salary growth" % (approx_salary_growth * 100))
  print("Repayment will take about  : %.1f years" % (repayment_months / 12.0))
  print("It will cost you total of  : %.0f GBP" % paid_total)
  print("You will pay extra         : %.0f GBP (%.0f%%)"
        % (paid_extra, paid_extra_percent))
  print("\nLoan values as time will go:")
  for year, loan_value in loan_values.items():
    print("  %d: %5.0f GBP" % (year, loan_value))

if __name__ == "__main__":
  main(sys.argv)
