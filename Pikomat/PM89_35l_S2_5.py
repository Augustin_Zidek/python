"""Pikomat, rocnik 35, leto, serie 2, priklad 5."""

import itertools
import sys

def count_matches(person_statement, col_assumption):
  assert len(person_statement.keys()) == 4
  assert len(col_assumption.keys()) == 4

  match_count = 0
  for key, values in person_statement.items():
    if col_assumption[key] in values:
      match_count += 1
  return match_count

def main(unused_args):
  a = {"kapuce": ["hneda"],
       "plast": ["bila"],
       "kalhoty": ["cervena"],
       "boty": ["seda"]}
  b = {"kapuce": ["cervena"],
       "plast": ["modra"],
       "kalhoty": ["ruzova"],
       "boty": ["bila"]}
  c = {"kapuce": ["cerna"],
       "plast": ["fialova"],
       "kalhoty": ["zluta"],
       "boty": ["modra"]}
  d = {"kapuce": ["bila"],
       "plast": ["bila"],
       # Negate "zluta" by listing all other possible options.
       "kalhoty": ["cervena", "ruzova", "zelena", "oranzova", "JINA"],
       "boty": ["bila"]}
  e = {"kapuce": ["zelena"],
       "plast": ["fialova"],
       "kalhoty": ["zelena"],
       "boty": ["seda"]}
  f = {"kapuce": ["seda"],
       "plast": ["modra"],
       "kalhoty": ["oranzova"],
       "boty": ["modra"]}
  person_statements = [a, b, c, d, e, f]

  cols_kapuce = ["hneda", "cervena", "cerna", "bila", "zelena", "seda", "JINA"]
  cols_plast = ["bila", "modra", "fialova", "zluta", "JINA"]
  cols_kalhoty = ["cervena", "ruzova", "zluta", "bila", "zelena", "oranzova", "JINA"]
  cols_boty = ["seda", "bila", "modra", "JINA"]

  for kapuce, plast, kalhoty, boty in itertools.product(
    cols_kapuce, cols_plast, cols_kalhoty, cols_boty):
    col_assumption = {"kapuce": kapuce,
                      "plast": plast,
                      "kalhoty": kalhoty,
                      "boty": boty}

    match_counts = [count_matches(s, col_assumption) for s in person_statements]
    # Check if the list contains only the number 1.
    if set(match_counts) == set([1]):
      print(col_assumption)


if __name__ == "__main__":
  main(sys.argv)
