"""Pikomat, rocnik 36, serie 4, priklad 5."""
import sys

def is_valid_time(digits):
  assert len(digits) == 4
  if digits[0] < 0 or digits[1] < 0 or digits[2] < 0 or digits[3] < 0:
    return False

  return 10 * digits[0] + digits[1] <= 24 and 10 * digits[2] + digits[3] <= 60


def is_valid_number(digits):
  return digits[0] > 0 and digits[1] >= 0 and digits[2] >= 0 and digits[3] >= 0


def get_digits(hours, minutes):
  digits = []
  digits.append(int(('%02d' % hours)[0]))
  digits.append(int(('%02d' % hours)[1]))
  digits.append(int(('%02d' % minutes)[0]))
  digits.append(int(('%02d' % minutes)[1]))
  return digits


def mirror_digit(digit):
  mirrored = {0: 0, 1: 1, 2: 5, 3: -1, 4: -1, 5: 2, 6: -1, 7: -1, 8: 8, 9: -1}
  return mirrored[digit]


def get_mirrored_digits(digits):
  return [mirror_digit(d) for d in reversed(digits)]


def format(digits):
  return '%d%d:%d%d' % (digits[0], digits[1], digits[2], digits[3])


def main(unused_args):
  mirrorable_times = 0
  valid_numbers = 0
  for hours in range(24):
    for minutes in range(60):
      digits = get_digits(hours, minutes)
      mirrored_digits = get_mirrored_digits(digits)

      if is_valid_time(mirrored_digits):
        mirrorable_times += 1
        print('%s -> %s' % (format(digits), format(mirrored_digits)))
      if is_valid_number(mirrored_digits):
        valid_numbers += 1

  print('Mirrorable times: %d' % mirrorable_times)
  print('Valid numbers: %d' % valid_numbers)

if __name__ == "__main__":
  main(sys.argv)
