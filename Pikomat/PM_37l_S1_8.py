"""Pikomat, rocnik 37, leto, serie 2, priklad 8."""

CLOSED = 1
OPEN = 0

def get_next_door(state, door_num):
  already_saw_one_closed = False
  for candidate_door in range(door_num + 1, door_num + len(state) + 1):
    candidate_door %= len(state)
    if state[candidate_door] == CLOSED:
      if already_saw_one_closed:
        return candidate_door
      else:
        already_saw_one_closed = True

def get_last_closed_door(state):
  for i, state in enumerate(state):
    if state == CLOSED:
      return i + 1
  raise ValueError('No open door!')


for num_doors in [47]:
  door_state = [CLOSED] * num_doors

  current_door = 0
  door_state[0] = OPEN
  while sum(door_state) != 1:
    next_door = get_next_door(door_state, current_door)
    door_state[next_door] = OPEN
    current_door = next_door

  print('Num doors: %d. Last open door: %d'
        % (num_doors, get_last_closed_door(door_state)))
