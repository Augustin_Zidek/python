"""Pikomat, rocnik 37, leto, serie 2, priklad 4."""
import itertools
import sys

import numpy as np

def create_board(c1, c2, c3, c4):
  board = np.zeros((4, 4), dtype=np.int32)
  board[c1] = 1
  board[c2] = 1
  board[c3] = 1
  board[c4] = 1
  return board

def check_board(board):
  if any(np.sum(board, axis=0) != 1):  # Horizontal.
    return False
  if any(np.sum(board, axis=1) != 1):  # Vertical.
    return False
  if np.sum(np.diagonal(board)) != 1:  # Main diagonal.
    return False
  if np.sum(np.diagonal(np.fliplr(board))) != 1:  # Anti-diagonal.
    return False
  return True

def main(unused_args):
  all_possible_coords = list(itertools.product([0, 1, 2, 3], [0, 1, 2, 3]))

  num_solutions = 0
  for (c1, c2, c3, c4) in itertools.combinations(all_possible_coords, 4):
    board = create_board(c1, c2, c3, c4)
    if check_board(board):
      print(board)
      num_solutions += 1
  print(f'Num solutions: {num_solutions}')


if __name__ == "__main__":
  main(sys.argv)
