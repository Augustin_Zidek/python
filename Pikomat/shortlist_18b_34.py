"""Priklad 18b.34"""

import sys

def shuffle(cards):
  assert len(cards) % 2 == 0
  new_cards = []
  for card1, card2 in zip(cards[:len(cards) // 2], cards[len(cards) // 2:]):
    new_cards.append(card1)
    new_cards.append(card2)
  return new_cards


def main(unused_args):
  cards = list(range(1, 53))
  shuffled = shuffle(cards)
  counter = 1
  while shuffled != cards:
    print(shuffled)
    shuffled = shuffle(shuffled)
    counter += 1
  print(counter)


if __name__ == "__main__":
  main(sys.argv)
