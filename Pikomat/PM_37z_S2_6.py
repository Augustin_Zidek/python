"""Pikomat, rocnik 37, zima, serie 2, priklad 6."""
import sys

_GAME_TABLE = {
    'K': {'K': 0, 'P': 0, 'N': 1},
    'P': {'K': 1, 'P': 0, 'N': 0},
    'N': {'K': 0, 'P': 1, 'N': 0},
}


def _format(array):
  return ''.join(array)


def game_result(symbol1, symbol2):
  return _GAME_TABLE[symbol1][symbol2], _GAME_TABLE[symbol2][symbol1]


def match_value(player1, player2):
  wins1 = 0
  wins2 = 0
  for symbol1, symbol2 in zip(player1, player2):
    result = game_result(symbol1, symbol2)
    wins1 += result[0]
    wins2 += result[1]
  return wins1, wins2


def is_better_score(score, other_score):
  score_value = score[0] - score[1]
  other_score_value = other_score[0] - other_score[1]
  return score_value < other_score_value


def get_best_score(scores):
  best_score = scores[0]
  best_score_index = 0
  for index, score in enumerate(scores):
    if is_better_score(best_score, score):
      best_score = score
      best_score_index = index
  return best_score, best_score_index


def play_match(player1, player2):
  permutations = [
      (player2[0], player2[1], player2[2]),
      (player2[0], player2[2], player2[1]),
      (player2[1], player2[0], player2[2]),
      (player2[1], player2[2], player2[0]),
      (player2[2], player2[0], player2[1]),
      (player2[2], player2[1], player2[0])]
  scores = [match_value(player1, p2_permutation) for p2_permutation in permutations]
  best_score, best_score_index = get_best_score(scores)
  match = ', '.join(['%s-%s' % (p1, p2) for p1, p2 in
                     zip(player1, permutations[best_score_index])])
  if best_score[0] - best_score[1] > 0:
    result = 'vyhrají kapři'
  elif best_score[0] - best_score[1] == 0:
    result = 'remíza'
  else:
    result = 'prohrají kapři'
  print('Kapři %s, štiky %s → %s %s %d:%d'
        % (_format(player1), _format(player2), match, result,
           best_score[0], best_score[1]))


def main(unused_args):
  play_match(['K', 'P', 'N'], ['K', 'K', 'K'])
  play_match(['K', 'P', 'N'], ['K', 'K', 'P'])
  play_match(['K', 'P', 'N'], ['K', 'K', 'N'])
  play_match(['K', 'P', 'N'], ['K', 'P', 'P'])
  play_match(['K', 'P', 'N'], ['K', 'P', 'N'])
  play_match(['K', 'P', 'N'], ['K', 'N', 'N'])
  play_match(['K', 'P', 'N'], ['P', 'P', 'P'])
  play_match(['K', 'P', 'N'], ['P', 'P', 'N'])
  play_match(['K', 'P', 'N'], ['P', 'N', 'N'])
  play_match(['K', 'P', 'N'], ['N', 'N', 'N'])


if __name__ == "__main__":
  main(sys.argv)
