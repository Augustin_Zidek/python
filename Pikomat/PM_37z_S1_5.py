"""Pikomat, rocnik 37, zima, serie 1, priklad 5."""
import sys

def is_pessimistic(num):
  digits = [int(x) for x in str(num)]

  prev_digit = digits[0]
  for d in digits[1:]:
    if prev_digit - 2 >= d:
      prev_digit = d
    else:
      return False
  return True


def main(unused_args):
  counter = 0
  for i in range(100000):
    if is_pessimistic(i):
      print(i)
      counter += 1

  print('Total: %d' % counter)


if __name__ == "__main__":
  main(sys.argv)
