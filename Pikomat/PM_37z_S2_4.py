"""Pikomat, rocnik 37, zima, serie 2, priklad 4."""
import itertools
import sys


_NUM_BRICKS = 15
_BRICK_VALUES = (2, 4, 7)

def value(counts):
  assert sum(counts) == _NUM_BRICKS
  return counts[0] * _BRICK_VALUES[0] + counts[1] * _BRICK_VALUES[1] + counts[2] * _BRICK_VALUES[2]


def main(unused_args):
  counts = [_NUM_BRICKS, 0, 0]

  unique_values = set()
  while True:
    unique_values.add(value(counts))

    if counts == [0, 0, _NUM_BRICKS]:
      break
    if counts[1] == 0:
      counts[0] -= 1
      counts[1] += 1 + counts[2]
      counts[2] = 0
    elif counts[1] > 0:
      counts[1] -= 1
      counts[2] += 1

  print(unique_values)
  print(len(unique_values))

if __name__ == "__main__":
  main(sys.argv)
