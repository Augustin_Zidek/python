"""Pikomat, rocnik 36, zima, serie 3, priklad 3."""
import sys

import numpy as np


class Board(object):

  def __init__(self, size):
    self._size = size
    self._board = np.zeros([size, size], dtype=np.bool_)

  def set_infected(self, i, j):
    self._board[i, j] = True

  def infect(self):
    for _ in range(self._size):
      for i in range(self._size):
        for j in range(self._size):
          infected_neighbours = 0
          if j > 0 and self._board[i, j - 1]:
            infected_neighbours += 1
          if j < self._size - 1 and self._board[i, j + 1]:
            infected_neighbours += 1
          if i > 0 and self._board[i - 1, j]:
            infected_neighbours += 1
          if i < self._size - 1 and self._board[i + 1, j]:
            infected_neighbours += 1
          if infected_neighbours >= 2 and not self._board[i, j]:
            self._board[i, j] = True

  def is_completely_infected(self):
    return np.all(self._board)

  def __str__(self):
    to_print = []
    for i in range(self._size):
      line = []
      for j in range(self._size):
        line.append('o' if self._board[i, j] else '.')
      line = ' '.join(line)
      to_print.append(line)
    return '\n'.join(to_print) + '\n'



def main(unused_args):
  b = Board(5)
  b.set_infected(0, 0)
  b.set_infected(1, 1)
  b.set_infected(2, 2)
  b.set_infected(3, 3)
  b.set_infected(4, 4)
  print(b)
  print(b.is_completely_infected())
  b.infect()
  print(b)
  print(b.is_completely_infected())

if __name__ == "__main__":
  main(sys.argv)
