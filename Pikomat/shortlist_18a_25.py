"""Priklad 18a.25"""

import sys

def digit_sum(n):
  r = 0
  while n:
    r, n = r + n % 10, n // 10
  return r


def main(unused_args):
  i = 234
  while i < 2019:
    i += digit_sum(i)
    print(i)

if __name__ == "__main__":
  main(sys.argv)
