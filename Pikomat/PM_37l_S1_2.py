"""Pikomat, rocnik 37, leto, serie 2, priklad 2."""
import itertools
import sys

def main(unused_args):
  people = {
      'ctvrtek': [4, 8, 12, 16, 20, 24, 28, 2, 6, 10, 14, 18, 22, 26, 30],
      '  patek': [5, 10, 15, 20, 25, 30],
      ' sestek': [6, 12, 18, 24, 30],
      '  sedma': [7, 14, 21, 28, 5, 12, 19, 26, 3, 10, 17, 24, 1, 8, 15, 22, 29,
                  6, 13, 20, 27, 4, 11, 18, 25, 2, 9, 16, 23, 30]
  }

  cnt = 0
  for i in range(1, 30):
    interval = [i, i + 1, i + 2, i + 3]
    interval = [x % 30 for x in interval]

    for (p1, p2, p3, p4) in itertools.permutations(people.keys(), 4):
      if (interval[0] in people[p1] and
          interval[1] in people[p2] and
          interval[2] in people[p3] and
          interval[3] in people[p4]):
        print(f'{cnt + 1}: {p1}={interval[0]}, {p2}={interval[1]}, '
              f'{p3}={interval[2]}, {p4}={interval[3]}')
        cnt += 1


if __name__ == "__main__":
  main(sys.argv)
