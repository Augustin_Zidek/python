from __future__ import print_function

for a in range(1, 16):
  for b in range(1, 16):
	  for c in range(1, 16):
		  if a + b + c == 15 and a <= b and b <= c and a + b > c and a + c > b and b + c > a:
			  print('(%d, %d, %d), ' % (a, b, c))