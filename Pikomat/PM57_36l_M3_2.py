"""Pikomat, rocnik 36, zima, serie 3, priklad 3."""
import sys

def is_palindrome(num):
  num_digits = list(str(num))
  for i in range(len(num_digits) // 2):
    if num_digits[i] != num_digits[len(num_digits) - i - 1]:
      return False
  return True

def is_ok(num):
  return '42' in str(num) or '24' in str(num)

def main(unused_args):
  for i in range(10000, 99999):
    if is_palindrome(i):
      if i % 12 == 0 and is_ok(i):
        print(i)

if __name__ == "__main__":
  main(sys.argv)
