"""Pikomat, rocnik 37, zima, serie 1, priklad 1."""
import sys

def modify(num):
  return int(str(num)[2:] + '8')



def main(unused_args):
  for i in range(10000, 100000):
    if modify(i) * 4 == i:
      print(i, modify(i))


if __name__ == "__main__":
  main(sys.argv)
