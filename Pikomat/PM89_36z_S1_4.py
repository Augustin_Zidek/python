"""Pikomat, rocnik 36, zima, serie 1, priklad 4."""

import sys


def all_perm_sum(num):
  assert 0 <= num < 1000
  c = str(num)[-1]
  b = ('0' + str(num))[-2]
  a = ('00' + str(num))[-3]

  if (a != b and a != c and b != c):
    return (int(a + b + c) + int(a + c + b) + int(b + a + c) +
            int(b + c + a) + int(c + a + b) + int(c + b + a))
  return -1


def main(unused_args):
  distinct_results = set()
  for i in range(100, 1000):
    distinct_results.add(all_perm_sum(i))

  print(sorted(distinct_results))
  print(len(distinct_results))

if __name__ == "__main__":
  main(sys.argv)
