"""Pikomat, rocnik 35, leto, serie 3, priklad 4."""

import sys


def is_prime(num):
  """Simple implementation of is_prime as speed is not a concern here."""
  if num == 1:
    return False
  for i in range(2, int(num**0.5 + 1)):
    if num % i == 0:
      return False
  return True


def count_matching(num):
  match_count = 0
  if num in [28, 29, 30, 32, 33, 34]:
    match_count += 1
  if num % 5 == 0 and num > 9 and num < 100:
    match_count += 1
  if num % 2 == 1:
    match_count += 1
  if '2' in str(num):
    match_count += 1
  if is_prime(num):
    match_count += 1
  if num in [x**2 for x in range(1, int(num**0.5 + 1))]:
    match_count += 1

  return match_count


def main(unused_args):
  for i in range(1, 100):
    if count_matching(i) == 3:
      print(i)


if __name__ == "__main__":
  main(sys.argv)
