from __future__ import print_function

def fact(num):
  prod = 1
  for i in range(2, num + 1):
	  prod *= i
  return prod


for i in range(1, 100):
  prod = fact(i)
  if prod % (i + 1) != 0:
    print(i + 1)