from __future__ import print_function

def to_num(a, b, c, d):
  return 1000*a + 100*b + 10*c + d


unique_res = set([])
for a in range(0, 10):
  for b in range(0, 10):
    abab = to_num(a, b, a, b)
    baba = to_num(b, a, b, a)
    diff = list(str(abab - baba))
    if len(diff) < 4:
      diff = [0] * (4 - len(diff)) + diff
    if len(diff) == 5:
      diff = diff[1:]
    if len(diff) == 4 and diff[0] == diff[2] and diff[1] == diff[3]:
      print('%d - %d = %d' % (abab, baba, abab - baba))
      unique_res.add(int(''.join(diff)))

print()
print(sorted(unique_res))
print()
print()

unique_res = set([])
for d in range(0, 10):
  for b in range(0, 10):
    diff = to_num(d, b, d, b) - to_num(b, d, b, d)
    diff = list(str(diff))
    if len(diff) < 4:
      diff = [0] * (4 - len(diff)) + diff
    if len(diff) == 5:
      diff = diff[1:]
    if len(diff) == 4 and diff[0] == diff[2] and diff[1] == diff[3] == str(d):
      print('D: %d, B: %d, diff: %s' % (d, b, ''.join(diff)))
      unique_res.add(int(''.join(diff)))

print()
print(sorted(unique_res))
