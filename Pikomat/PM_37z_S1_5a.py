"""Pikomat, rocnik 37, zima, serie 1, priklad 5 -- nová verze."""
import sys

def num_digits(num):
  return len(str(num))


def main(unused_args):
  num = 0
  total_digits = 0
  while True:
    num += 1
    total_digits += num_digits(num)
    if total_digits == 3 * num:
      print(num, total_digits)
      break


if __name__ == "__main__":
  main(sys.argv)
