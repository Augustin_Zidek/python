"""Pikomat, rocnik 36, zima, serie 2, priklad 5."""

import itertools
import sys


def contains_group(array, divisor):
  for index in range(len(array)):
    for length in range(1, len(array) - index + 1):
      if sum(array[index:index + length]) % divisor == 0:
        return True
  return False


def test_all_combinations(length):
  for array in itertools.product(range(length), repeat=length):
    if not contains_group(list(array), divisor=length):
      return False
  return True


def main(unused_args):
  print(test_all_combinations(10))


if __name__ == "__main__":
  main(sys.argv)
