"""MatX 2019, priklad 34."""
import sys
import itertools


def main(unused_args):
  women = [('A', 1220), ('I', 1320), ('D', 1420)]
  men = [('P', lambda x: 2 * x), ('E', lambda x: x), ('M', lambda x: 1.5 * x)]
  couple = lambda woman, man: woman[1] + man[1](woman[1])

  for women_perm in itertools.permutations(women):
    if sum([couple(w, m) for w, m in zip(women_perm, men)]) == 10000:
      print([w[0] + '+' + m[0] for w, m in zip(women_perm, men)])


if __name__ == "__main__":
  main(sys.argv)
