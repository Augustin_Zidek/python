"""MatX 2019, priklad 13."""
import sys


def is_happy(num):
  return num % 17 == 0


def is_good(num):
  return num % 13 == 0


def main(unused_args):
  counter = 0
  for i in range(1, 20000):
    if is_happy(i) and is_good(i):
      counter += 1

  print(counter)

if __name__ == "__main__":
  main(sys.argv)
