"""MatX 2019, priklad 50."""
import sys


def gcd(a, b):
  if b == 0:
    return a
  return gcd(b, a % b)


def main(unused_args):
  counter = 0
  for i in range(1, 1261):
    if gcd(i, 1260) == 1:
      counter += 1
  print(counter)


if __name__ == "__main__":
  main(sys.argv)
