from __future__ import print_function

import collections

def digit_histogram(number):
  return collections.Counter(str(number))


for a in xrange(1000000000, 6210001000 + 1):
  if a % 1000000 == 0:
    print("At %d" % a)
  hist = digit_histogram(a)
  is_special = True
  for i, digit in enumerate(str(a)):
    if hist[str(i)] != int(digit):
      is_special = False
      break
  if is_special:
    print(a)
