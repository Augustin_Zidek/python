from __future__ import print_function

for a in xrange(10000, 99999):
  if 3 * (100000 + a) == 10 * a + 1:
    print(a)
