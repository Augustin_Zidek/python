"""MatX 2019, priklad 3."""
import sys


def to_digits(num):
  return [int(d) for d in str(num)]


def is_palindrome(num_digits):
  for i in range(len(num_digits) // 2):
    if num_digits[i] != num_digits[len(num_digits) - i - 1]:
      return False
  return True


def main(unused_args):
  counter = 0
  for i in range(100000, 999999):
    i_digits = to_digits(i)
    if (i % 6 == 0 and
        is_palindrome(i_digits) and
        i_digits[0] == i_digits[1] + 3 and
        i_digits[1] == i_digits[2] + 3):
      counter += 1
      print(i)
  print(counter)

if __name__ == "__main__":
  main(sys.argv)
