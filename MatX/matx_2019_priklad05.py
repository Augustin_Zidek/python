"""MatX 2019, priklad 5."""
import sys


def to_digits(num):
  return [int(d) for d in str(num)]


def next_bad(num):
  return num + sum(to_digits(num))


def main(unused_args):
  bad_year = 234
  while bad_year <= 2019:
    bad_year = next_bad(bad_year)
    print(bad_year)

if __name__ == "__main__":
  main(sys.argv)
