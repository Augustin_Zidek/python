from __future__ import print_function

counter = 0


def is_happy(num):
  return num % 2 == 0 or num % 3 == 0 or num % 5 == 0 or num % 7 == 0


for a in xrange(1, 101):
  if not is_happy(a):
    print("Not happy: ", a)
    counter += 1

print(counter)
