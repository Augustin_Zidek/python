"""Implementation of the Enigma encryption machine."""

import sys
import string


def letter_to_num(letter):
  return ord(letter.upper()) - ord('A')


def num_to_letter(num):
  return chr(num + ord('A'))


def invert(array):
  inverted = [0]*len(array)
  for i, a in enumerate(array):
    inverted[a] = i
  return inverted


class Scrambler(object):
  def __init__(self, substitution):
    assert set(substitution) == set(string.ascii_uppercase)
    self._wiring_forward = []
    for letter_to in substitution:
      self._wiring_forward.append(letter_to_num(letter_to))
    self._wiring_backward = invert(self._wiring_forward)
    self._rotation = 0

  def rotate(self, amount=1):
    assert amount >= 0
    self._wiring_forward = (self._wiring_forward[amount:] +
                            self._wiring_forward[:amount])
    self._wiring_backward = invert(self._wiring_forward)
    self._rotation += amount
    carry = self._rotation // 26
    self._rotation = self._rotation % 26
    return carry

  def scramble_forward(self, char):
    return num_to_letter(self._wiring_forward[letter_to_num(char)])

  def scramble_backward(self, char):
    return num_to_letter(self._wiring_backward[letter_to_num(char)])

  def set_state(self, letter):
    assert len(letter) == 1
    self.rotate(26 - self._rotation)
    self.rotate(letter_to_num(letter))

  def get_state(self):
    return num_to_letter(self._rotation)

  def __str__(self):
    representation = []
    for num_from, num_to in enumerate(self._wiring_forward):
      representation.append(
          '%s->%s' % (num_to_letter(num_from), num_to_letter(num_to)))
    return ', '.join(representation)


class Enigma(object):
  """Enigma encryption/decryption class."""

  def __init__(self, scramblers, reflector, substitutions, initial_state):
    self._scramblers = scramblers
    self._reflector = reflector
    self._substitutions = substitutions
    self.set_state(initial_state)

  def _rotate_scramblers(self):
    carry = self._scramblers[-1].rotate(amount=1)
    for i in range(2, len(self._scramblers) + 1):
      carry = self._scramblers[-i].rotate(carry)

  def encrypt_single_char(self, char):
    scrambled_char = char
    # Forward pass.
    for scrambler in reversed(self._scramblers):
      scrambled_char = scrambler.scramble_forward(scrambled_char)
    # Reflector (to achieve encryption-decryption symmetry).
    scrambled_char = self._reflector.scramble_forward(scrambled_char)
    # Backward pass.
    for scrambler in self._scramblers:
      scrambled_char = scrambler.scramble_backward(scrambled_char)
    self._rotate_scramblers()
    return scrambled_char

  def encrypt(self, text):
    encrypted = []
    for char in text.upper():
      encrypted.append(self.encrypt_single_char(char))
    return ''.join(encrypted)

  def decrypt(self, text):
    return self.encrypt(text)

  def set_state(self, state):
    assert len(self._scramblers) == len(state)
    for i, scrambler_state in enumerate(state):
      self._scramblers[i].set_state(scrambler_state)

  def get_state(self):
    return ''.join([s.get_state() for s in self._scramblers])


def main(unused_args):
  s1 = Scrambler('EKMFLGDQVZNTOWYHXUSPAIBRCJ')
  s2 = Scrambler('AJDKSIRUXBLHWTMCQGZNPYFVOE')
  s3 = Scrambler('BDFHJLCPRTXVZNYEIWGAKMUSQO')
  s4 = Scrambler('ESOVPZJAYQUIRHXLNFTGKDCMWB')
  s5 = Scrambler('VZBRGITYUPSDNHLXAWMJQOFECK')
  r =  Scrambler('YRUHQSLDPXNGOKMIEBFZCWVJAT')

  enigma = Enigma(
      scramblers=[s3, s2, s1], reflector=r, substitutions=None,
      initial_state='KEY')
  encrypted = enigma.encrypt('HELLOWORLD')
  print(encrypted)
  enigma.set_state('KEY')
  print(enigma.decrypt(encrypted))


if __name__ == "__main__":
  main(sys.argv)
